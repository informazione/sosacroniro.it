<!DOCTYPE html>
<html lang="en">

  <head>
  
  <link rel="stylesheet" type="text/css" href="css/main.css" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <link rel="stylesheet" type="text/css" href="css/textsett.css" />
  <link rel="stylesheet" type="text/css" href="css/nav.css" />
  <link rel="stylesheet" type="text/css" href="css/media.css" />
  <title>Marst Web Student </title>
  <meta charset="UTF-8"> 
  <meta name="description" content="">
  <script src="scripts/jquery_1.8.3-min.js" type="text/javascript"></script>
  <script type="text/javascript" src="scripts/jquery.aw-showcase.min.js"></script>
  <script type="text/javascript" src="scripts/jquery_easing.js"></script>
  <script type="text/javascript" src="scripts/scripts.js"></script>
  
  <!--[if IE]>

<style>

.menu {width: 600px;}

  </style>

<![endif]-->


  </head>
  
  <body>
   
   <div id="wrapper">
   <div id="container">

    <header class="upper">
     
    <!--  </h1> -->
     
     <nav>
       <ul class="menu">
         
         <li><a href="#about"><p>About</p></a></li> 
         <li><a href="#portfolio"><p>Portfolio</p></a></li>
         <li><a href="#contact"><p>Contact me</p></a></li>

       </ul>
     </nav>	
  
    </header>


    <div id="main">
      
      <h1 id="logo" class="logo"></h1> <img src="css/images/slog.png" id="slog" class="slog" alt="" /> 
     
      <article>

      	<!-- ABOUT -->
       <div></div>
        <header class="lower">
        <h2 class="sec_title" id="about">About</h2>  
        </header>  
        
        <section class="content">
           <p> My name is Marco Stinco, i study web design at Synergia s.r.l., in Bologna. This is not my real web page, but just an assignment given me by my teacher Matteo Magni.
            I aim to build the real one in a very near future, though i have to study before that. </p>
            <img id="myphoto" src="css/images/io.png" alt="My photo" />
        </section>

        <section class="content">

           <p> I was born in Trapani (Sicily) in 1991. I have a public High School degree, got in July 2010. I've no work experience yet, but i hope i find a good job soon.
           I like a vast amount of things, most of them even not well defined. I like to watch movies, those which can teach you something, cooking, when i have enough money to buy the right ingriedents, listen to music (no preferred genre),
           playing videogames, and such other things the majority of people do.</p>
           <p>
           And i love winter.
           </p> 
                       
        </section>      
        
        <section class="content">

           <p>But the thing i love the most is Internet. You can find anything you want on the Web, information, movies, videos, books, music, images, and, hell, even people.
              For me, i passed so much time on the Net, that i can't even remember when i began to. Somewhere around 9 or 10 of age, i think, but since then, 
              the first thing i do every morning is typing "www". 
            </p> 
                       
        </section> 

        <section class="content">

           <p> That's why i want to be part of it. I want to do my part to do make the Web grow bigger, make it evolve, and be more beautiful.
               That's why i decided to design as Winter would. 
           </p> <p> Making it cool. </p>
                       
        </section> 
        
        <!-- PORTFOLIO -->
        
        <header class="lower">
        <h2 class="tir_title" id="portfolio">Portfolio</h2> 
        </header> 
        

        <div id="showcase" class="showcase">
        <!-- Each child div in #showcase represents a slide -->
        <div class="showcase-slide">
                <!-- Put the slide content in a div with the class .showcase-content -->
                <div class="showcase-content">
                        <!-- If the slide contains multiple elements you should wrap 
                      them in a div with the class .showcase-content-wrapper. 
                        We usually wrap even if there is only one element, because it looks better. :-) -->
                        <div class="showcase-content-wrapper">
                                <img src="css/images/03.jpg" alt="01" />
                                
                        </div>

                </div>
                <!-- Put the caption content in a div with the class .showcase-caption -->
                <div class="showcase-caption">
                        <a href="http://www.sosacroniro.it"> Sosacroniro.it</a>
                </div>
                <!-- Put the tooltips in a div with the class .showcase-tooltips. -->
                <div class="showcase-tooltips">
                        <!-- Each anchor in .showcase-tooltips represents a tooltip. 
                      The coords attribute represents the position of the tooltip. -->
                       
                                <!-- The content of the anchor-tag is displayed in the tooltip. -->
                               
                                             
                </div>
        </div>
        <div class="showcase-slide">
                <div class="showcase-content">
                        <div class="showcase-content-wrapper">
                                <img src="css/images/02.jpg" alt="01" />
                        </div>
                </div>
              
              <div class="showcase-caption">
                        <a href="http://www.italianflairopen.it/"> ItalianFlairOpen.it</a>
                </div>
                <!-- Put the tooltips in a div with the class .showcase-tooltips. -->
                <div class="showcase-tooltips">
                        <!-- Each anchor in .showcase-tooltips represents a tooltip. 
                      The coords attribute represents the position of the tooltip. -->
                        
                                <!-- The content of the anchor-tag is displayed in the tooltip. -->
                       
                        
                </div>

        </div>
</div>  <!--showcase end-->

<p class="showfoot">This showcase is a creation by the <a href="http://www.awkwardgroup.com/"> Awkward Group </a>. Visit their website, please. </p> 
<p class="showfoot">Not showing for resolutions under 480 x 320. If on mobile device, try tilting it horizontally.</p>
        <!-- CONTACT ME -->
        

        <header class="lower">
                 <h2 class="sec_title" id="contact">Contact me</h2> 
        </header> 

        <section class="contacts">
              
               <form  action="scripts/form_function.php" method="post" onsubmit="return validateThisForm(this)">
                        <h3 class="formtit">Fill in the form!</h3>
                        <p class="input1"><label for="nome">Name</label><input name="nome" type="text" id="nome" class="nome" placeholder="Mr. Jethro Tull" /></p>
                        <p><label for="email">Email &nbsp;</label><input name="email" type="text" id="email" class="email" placeholder="ob1Kenobi@example.com" /></p>
                        <p><label for="tel">Phone</label><input name="tel" type="text" id="tel" class="tel" placeholder="+XX XXX - 456789" /></p>
                        <p><label class="stupid" for="testo">Mess.</label><textarea name="testo" id="testo" class="testo" placeholder="Write your message..."></textarea></p>
                        <p><input value="Send" type="submit" id="Send"  class="button"/>
                           <span id="formalert"></span>
                        </p>


                </form>

       </section>  

      

               <aside class="infoas">
                    <h3 class="asidetit">Use the classic way...</h3>
                     <p>
 
                      <span class="fstop"> Marco Stinco</span>                                            
                      <span class="fstop"> Bologna, Italy</span>   
                      <span class="fstop"> 389 - 4832232</span>   
                      <span class="fstop"> <a href="mailto:marstweb@gmail.com">marstweb@gmail.com</a></span>   
                     </p>
   

                     <h3 class="asidetit">...or go social!</h3>
                      <p>

                        <span class="fstop"> <a href="http://www.facebook.com/Lead951"  target="_blank">Facebook</a></span>                                            
                        <span class="fstop"> <a href="https://twitter.com/Lead915"  target="_blank">Twitter</a></span>  
                        <span class="fstop"> <a href="http://www.linkedin.com/profile/view?id=219885415&amp;trk=hb_tab_pro_top" target="_blank">Linkedin</a></span>   
                      
                      </p>
                </aside>



       
       
      </article>
   
    </div> <!--main end-->


    <div id="footer">
       
       <footer>
       	

       	<section class="downlogo">
       		<img src="css/images/marst2.png" alt="" />
       		<p class="fuck"><a href="#main">To top </a></p>
       	</section>

       	<section class="links">
   
        <a  href="http://www.facebook.com/share.php?u=http://www.sosacroniro.it/people/marco/index.php" class="facebook-button" ><img src="css/images/facebook.png" alt="" /></a>
        <a  href="https://twitter.com/Lead915"  target="_blank" class="twitter-follow-button" data-show-count="false" data-lang="it"><img src="css/images/twitter.png" alt="" /></a>
        <a  href="" class="linkedin-button"><img src="css/images/linkedin.png" alt="" /></a>
       
       	</section>

       	
       	 
       	<section class="copy">
       		<p>This website and all its content, design and code  (except for Jquery plugins) are &copy;2012 Marco Stinco. The code contains valid HTML and CSS.
       	</section> 
       </footer>

 
    </div> <!--footer end-->

      
   </div> <!--container end-->
   </div> <!--wrapper end-->
  </body>

</html>	