//SCRIPT GOOGLE ANALYTICS

var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33399894-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

	


// COLOR CHANGE

	

    $('#green').click(function() {


      $('body').css('background', '#74b539');
      $('#logo').css('background', 'url(css/images/logo_blue.png)' )

      $('.info').css('background', '#FFFFFF url(css/images/info_green.png) no-repeat center');
      $('.info').css('background-size', '70%');
      $('.foto').css('background', '#FFFFFF url(css/images/camera_green.png) no-repeat center');
      $('.foto').css('background-size', '70%');
      $('.ordina').css('background', '#FFFFFF url(css/images/box_green.png) no-repeat center');
      $('.ordina').css('background-size', '70%');
      $('.contatti').css('background', '#FFFFFF url(css/images/mail_green.png) no-repeat center');
      $('.contatti').css('background-size', '70%');

      $('.menu a').css('color', '#74b539');
      $('.title_left').css('background', '#FFFFFF url(css/images/info_blue.png) no-repeat center');
      $('.title_left').css('background-size', '70%');
      $('h2').css('color', '#4264f7');
      $('#mobile1 h3').css('color', '#4264f7');
      
      $('#spectec').css('background', 'url(css/images/spectec_blue.png)');
      $('#spectec').css('background-size', '100%');
      $('#socfon').css('background', 'url(css/images/socfon_blue.png)');
      $('#socfon').css('background-size', '100%');

      $('.handle').css('background', 'url(css/images/splabel_blue.png) no-repeat');
      $('.handle2').css('background', 'url(css/images/sclabel_blue.png) no-repeat');
      $('.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default').css('background','#4264f7 url(images/ui-bg_flat_75_74b539_40x100_blue.png) 50% 50% repeat-x');
      

      $('.middle1, .middle2').css('background', '#4264f7');
     // $('.title_middle').attr("src","css/images/");
      
      $('.title_left2').css('background', ' url(css/images/box_blue.png) no-repeat ');
      $('.title_left2').css('background-size', '70%');
      
      $('.button').css('color', '#4264f7');
      $('.copy').css('color', '#4264f7');
      $('.link').css('color', '#4264f7');

    });

    $('#red').click(function() {


      $('body').css('background', '#f74242');
      $('#logo').css('background', 'url(css/images/logo_purple.png)' ) //opp

      $('.info').css('background', '#FFFFFF url(css/images/info_red.png) no-repeat center');
      $('.info').css('background-size', '70%');
      $('.foto').css('background', '#FFFFFF url(css/images/camera_red.png) no-repeat center');
      $('.foto').css('background-size', '70%');
      $('.ordina').css('background', '#FFFFFF url(css/images/box_red.png) no-repeat center');
      $('.ordina').css('background-size', '70%');
      $('.contatti').css('background', '#FFFFFF url(css/images/mail_red.png) no-repeat center');
      $('.contatti').css('background-size', '70%');

      $('.menu a').css('color', '#f74242');
      $('.title_left').css('background', '#FFFFFF url(css/images/info_purple.png) no-repeat center'); //opp
      $('.title_left').css('background-size', '70%');
      $('h2').css('color', '#92278f'); //opp
      $('#mobile1 h3').css('color', '#92278f'); //opp
      
      $('#spectec').css('background', 'url(css/images/spectec_purple.png)'); //opp
      $('#spectec').css('background-size', '100%');
      $('#socfon').css('background', 'url(css/images/socfon_purple.png)'); //opp
      $('#socfon').css('background-size', '100%');

      $('.handle').css('background', 'url(css/images/splabel_purple.png) no-repeat');   //opp
      $('.handle2').css('background', 'url(css/images/sclabel_purple.png) no-repeat');  //opp
      $('.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default').css('background','#92278f url(images/ui-bg_flat_75_74b539_40x100_purple.png) 50% 50% repeat-x');
      //opp

      $('.middle1, .middle2').css('background', '#92278f');
     // $('.title_middle').attr("src","css/images/");
      
      $('.title_left2').css('background', ' url(css/images/box_purple.png) no-repeat '); //opp
      $('.title_left2').css('background-size', '70%');
      
      $('.button').css('color', '#92278f'); //opp
      $('.copy').css('color', '#92278f');  //opp
      $('.link').css('color', '#92278f'); //opp

    });

     $('#purple').click(function() {


      $('body').css('background', '#92278f');
      $('#logo').css('background', 'url(css/images/logo_red.png)' ) //opp

      $('.info').css('background', '#FFFFFF url(css/images/info_purple.png) no-repeat center');
      $('.info').css('background-size', '70%');
      $('.foto').css('background', '#FFFFFF url(css/images/camera_purple.png) no-repeat center');
      $('.foto').css('background-size', '70%');
      $('.ordina').css('background', '#FFFFFF url(css/images/box_purple.png) no-repeat center');
      $('.ordina').css('background-size', '70%');
      $('.contatti').css('background', '#FFFFFF url(css/images/mail_purple.png) no-repeat center');
      $('.contatti').css('background-size', '70%');

      $('.menu a').css('color', '#92278f');
      $('.title_left').css('background', '#FFFFFF url(css/images/info_red.png) no-repeat center'); //opp
      $('.title_left').css('background-size', '70%');
      $('h2').css('color', '#f74242'); //opp
      $('#mobile1 h3').css('color', '#f74242'); //opp
      
      $('#spectec').css('background', 'url(css/images/spectec_red.png)'); //opp
      $('#spectec').css('background-size', '100%');
      $('#socfon').css('background', 'url(css/images/socfon_red.png)'); //opp
      $('#socfon').css('background-size', '100%');

      $('.handle').css('background', 'url(css/images/splabel_red.png) no-repeat');   //opp
      $('.handle2').css('background', 'url(css/images/sclabel_red.png) no-repeat');  //opp
      $('.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default').css('background','#f74242 url(images/ui-bg_flat_75_74b539_40x100_red.png) 50% 50% repeat-x');
      //opp

      $('.middle1, .middle2').css('background', '#f74242'); //opp
     // $('.title_middle').attr("src","css/images/");
      
      $('.title_left2').css('background', ' url(css/images/box_red.png) no-repeat '); //opp
      $('.title_left2').css('background-size', '70%');
      
      $('.button').css('color', '#f74242'); //opp
      $('.copy').css('color', '#f74242');  //opp
      $('.link').css('color', '#f74242'); //opp

    });

    $('#blue').click(function() {


      $('body').css('background', '#4264f7');
      $('#logo').css('background', 'url(css/images/logo_green.png)' ) //opp

      $('.info').css('background', '#FFFFFF url(css/images/info_blue.png) no-repeat center');
      $('.info').css('background-size', '70%');
      $('.foto').css('background', '#FFFFFF url(css/images/camera_blue.png) no-repeat center');
      $('.foto').css('background-size', '70%');
      $('.ordina').css('background', '#FFFFFF url(css/images/box_blue.png) no-repeat center');
      $('.ordina').css('background-size', '70%');
      $('.contatti').css('background', '#FFFFFF url(css/images/mail_blue.png) no-repeat center');
      $('.contatti').css('background-size', '70%');

      $('.menu a').css('color', '#4264f7');
      $('.title_left').css('background', '#FFFFFF url(css/images/info_green.png) no-repeat center'); //opp
      $('.title_left').css('background-size', '70%');
      $('h2').css('color', '#74b539'); //opp
      $('#mobile1 h3').css('color', '#74b539'); //opp
      
      $('#spectec').css('background', 'url(css/images/spectec_green.png)'); //opp
      $('#spectec').css('background-size', '100%');
      $('#socfon').css('background', 'url(css/images/socfon_green.png)'); //opp
      $('#socfon').css('background-size', '100%');

      $('.handle').css('background', 'url(css/images/splabel_green.png) no-repeat');   //opp
      $('.handle2').css('background', 'url(css/images/sclabel_green.png) no-repeat');  //opp
      $('.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default').css('background','#74b539 url(images/ui-bg_flat_75_74b539_40x100.png) 50% 50% repeat-x');
      //opp

      $('.middle1, .middle2').css('background', '#74b539'); //opp
     // $('.title_middle').attr("src","css/images/");
      
      $('.title_left2').css('background', ' url(css/images/box_green.png) no-repeat '); //opp
      $('.title_left2').css('background-size', '70%');
      
      $('.button').css('color', '#74b539'); //opp
      $('.copy').css('color', '#74b539');  //opp
      $('.link').css('color', '#74b539'); //opp

    });





// FONT SIZE CHANGER

	
function setFontSize(size) {
var body = document.getElementsByTagName('body')[0];
var dimensione = "12px"; // default
if (size == 1) dimensione = "8px";
if (size == 2) dimensione = "10px";
if (size == 3) dimensione = "12px";
if (size == 4) dimensione = "14px";
if (size == 5) dimensione = "16px";
body.style.fontSize = dimensione;
}

window.onload = function() {
setFontSize(size);
}

//  TOPSECTION

	    $(document).ready(function() {
      			        $( "#topsection" ).mouseleave(function(){$(this).fadeOut(500);});
        			      $( "#hidetopsection" ).mouseenter(function(){$("#topsection").fadeIn(500);});
       			    });
	   




// SCROLL EASING (ANCHOR)

$(function() {
    $('.menu a , .ease').bind('click',function(event){
        var $anchor = $(this);
 
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1200,'easeInOutExpo');
        /*
        if you don't want to use the easing effects:
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1000);
        */
        event.preventDefault();
    });
}); 



// FLEXSLIDER

$(window).load(function() {
    $('.flexslider').flexslider();
  });

// CONTACT PANEL FUNCTION

$(function() {
        $( "#contact_panel" ).accordion();
    });





   // INFO TAB FUNCTION
 
$(document).ready(function() {
        $( "#tabs" ).tabs();
    });

 // INFO TABLE SORTER FUNCTION
  $(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
); 
  
  // CONTACT FORM VALIDATION


var nome = document.getElementById('nome');
   var email = document.getElementById('email');
   var tel = document.getElementById('tel');
   var testo = document.getElementById('testo');



function validateThisForm(form) { 


if (form.nome.value == "") { 
   document.getElementById('formalert').innerHTML = '*Non Hai inserito il tuo nome!';
   form.nome.focus( ); 
   return false; 
   } else if (form.nome.value.length > 0 && form.nome.value.length < 3) { 
   document.getElementById('formalert').innerHTML = '*Il nome deve essere di minimo 3 caratteri!';
   form.nome.focus( ); 
   return false; 
   };


if (form.email.value == "") { 
   document.getElementById('formalert').innerHTML = '*Hai dimenticato la tua email!';
 
   form.email.focus( ); 
   return false; 
   };

if (form.tel.value == "") { 
   document.getElementById('formalert').innerHTML = '*Hai dimenticato il numero telefonico!';
 
   form.email.focus( ); 
   return false; 
   };


if (form.testo.value == "") { 
   document.getElementById('formalert').innerHTML = '*Non ha inserito il tuo messaggio!';
  
   form.testo.focus( ); 
   return false; 
   };

};
 

 // MOBILE

$(window).load(function() {
   
   var windowSize = $(window).width();
  if (windowSize < 641) {

    $('#tabs-cont').hide();
    $('#sort').hide();
    $('#spectec').hide();
    $('#socfon').hide();
    $('#mobile1').show();
    $('#formdiv').hide();

  } else { 

    $('#tabs-cont').show();
    $('#sort').show();
    $('#spectec').show();
    $('#socfon').show();
    $('#mobile1').hide();
    $('#formdiv').show();
  };

});

$(window).resize(function() {
 
  var windowSize = $(window).width();
  if (windowSize < 641) {

    $('#tabs-cont').hide();
    $('#sort').hide();
    $('#spectec').hide();
    $('#socfon').hide();
    $('#mobile1').show();
    $('#formdiv').hide();

  } else { 

   $('#tabs-cont').show();
    $('#sort').show();
    $('#spectec').show();
    $('#socfon').show();
    $('#mobile1').hide();
    $('#formdiv').show();
  };

}); 





