<!DOCTYPE HTML>
<html lang="it">

<head>

<link rel="stylesheet" type="text/css" href="css/main_min.css"/>
<link rel="stylesheet" type="text/css" href="css/custom.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.9.2.custom.min.css"/>
<link rel="stylesheet" type="text/css" href="css/flexslider.css"/>
<link rel="stylesheet" type="text/css" href="css/tablesorter/tablesorter.css"/>
<meta http-equiv="Content-Type" content="text/html;      charset=UTF-8" />
<title>Sosacroniro</title>
<link rel="shortcut icon" type="image/ico" href="favicon.ico" />
<meta name="description" content="Sosacroniro, è il dispositivo mobile ripiegabile che incorporerà le funzioni di un orologio da polso, di uno smartphone, di un tablet, e di un netbook." />
<meta name="keywords" content="sosacroniro, smartphone, tablet, netbook, sosacron" />
<meta name="viewport" content="width=device-width" />
<script src="scripts/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="scripts/jquery-ui-1.9.2.custom.min.js"></script>
<script src="scripts/jquery.tablesorter.min.js"></script>
<script src="scripts/jquery-easing.js"></script>
<script src="scripts/jquery.flexslider-min.js"></script>

<script type="text/javascript" src="scripts/scripts.js"></script>





</head>

<body>

<div id="wrapper">
  
  <div id="hidetopsection">Custom</div>
		<div id="topsection">
        		<div id="fontsize">
                	<span class="font8">Dimensione carattere:</span>
                	<span class="font8"><a href="javascript: setFontSize(1)">A </a></span>
                	<span id="font10"><a href="javascript: setFontSize(2)">A </a></span>
                	<span id="font12" class="redfont"><a href="javascript: setFontSize(3)">A </a></span>
                	<span id="font14"><a href="javascript: setFontSize(4)">A </a></span>
                	<span id="font16"><a href="javascript: setFontSize(5)">A</a></span>
                    <br/>
                    <br/>
                    <span class="font8">Colore sito:</span>
                    <span id="verde" class="verde"><a href="#" id="green" onclick="" >
                    <img src="css/images/verde.gif" width="20" height="20" alt="pulsante verde" /></a></span>                    
                	<span id="rosso" class="rosso"><a href="#" onclick="" id="red">
                	<img src="css/images/rosso.gif" width="20" height="20" alt="pulsante rosso" /></a></span>
                	<span id="viola" class="viola"><a href="#" onclick="" id="purple">
                	<img src="css/images/viola.gif" width="20" height="20" alt="pulsante viola" /></a></span>
					<span id="blu" class="blu"><a href="#" onclick="" id="blue">
                	<img src="css/images/blu.gif" width="20" height="20" alt="pulsante blu" /></a></span>
                </div>
       	</div>


 <div id="logo"></div>
    
     <h1> Pure Future. Pure Power.</h1>
  
     <nav class="menu">
     
         <a href="#info" class="info" id="infonav" ><p>Info</p></a>
         <a href="#foto" class="foto" id="fotonav"><p>Foto</p></a>
         <a href="#ordina" class="ordina" id="ordinav"><p>Ordina</p></a>
         <a href="#contatti" class="contatti" id="contanav"><p>Contatti</p></a>

     </nav>
   
<div class="main">
<a href="#info" class="ease"><img src="css/images/arrowdown.gif" alt="down" class="arrowdown" ></a>
   
   <article>
      
      <section class="left1" id="first">
       
       <header  class="lefthead"> <h2 class="title_left" > </h2> </header> 
    
       <h2 class="argom" id="info">Cosa è Sosacroniro</h2>
      <p class="firp"><strong>Sosacroniro</strong> è un prodotto rivoluzionario, destinato a conquistare il mercato dell'elettronica di consumo.</p>
      
        <p><strong>Sosacroniro</strong> è più di uno smartphone, più di un semplice dispositivo elettronico: è il futuro.
Il materiale di cui è costituito, il <strong>Sosacron</strong>, una variante del <a class="link" href="http://it.wikipedia.org/wiki/Grafene">grafene</a>, ha delle caratteristiche tali da soppiantare gli ormai obsoleti <a class="link" href="http://it.wikipedia.org/wiki/Circuito_stampato">circuiti stampati al silicio</a>.</p>

<p class="lasp">Malleabile, flessibile, ultra-resistente, trasparente; il Sosacron rende <strong>Sosacroniro</strong> in grado di trasformarsi a scelta in <strong>orologio da polso</strong>, <strong>smartphone</strong>, <strong>tablet</strong>, o <strong>netbook</strong>,
facendone il dispositivo mobile definitivo.</p>
      

      <div id="spectec"> </div>
      <div id="socfon">  </div>
      
      <div id="tabs-cont">
        <div id="tabs">  <!-- TABS OPEN-->
    <ul>
        <li><a href="#tabs-1">DISPLAY</a></li>
        <li><a href="#tabs-2">HARDWARE</a></li>
        <li><a href="#tabs-3">SOFTWARE</a></li>
        <li><a href="#tabs-4">CONNETTIVIT&Agrave;</a></li>
        <li><a href="#tabs-5">FOTOCAMERA</a></li>
    </ul>
    <div id="tabs-1">
        <ul><li><strong>Display</strong> superAMOLED total surface IPS multi-touch capacitivo</li>
        	<li><strong>Dimensioni</strong> da 2'', 4.3'', 9.7'' e 13'' (rispettivamente orologio, smartphone, tablet, e netbook)</li> 
        	<li><strong>Risoluzione</strong> massima 1440 x 900 a 712 ppi</li></ul> 
    </div>
    <div id="tabs-2">
         <ul>
         <li><strong>Processore</strong>: ANF Graphite MD1 12 GHz 9-core</li>
         <li><strong>Memoria RAM</strong>: 16 Gb</li>
         <li><strong>Hard disk</strong>: 2 Tb</li>
         <li><strong>Batteria</strong>: Celle di Grafene ad H20</li>
        </ul>
    </div>
    <div id="tabs-3">
        <ul><li><strong>O/S</strong> Chron Futura YZ</li></ul>
        
    </div>
    <div id="tabs-4">
        <ul>
<li><strong>Wi-Fi</strong></li>
<li><strong>GPS</strong></li>
<li><strong>4G SLTE</strong></li>
<li><strong> Bluetooth 4.0</strong></li>
</ul>
    </div>
    <div id="tabs-5">
        <ul><li><strong>Principale</strong>: 60 Megapixel</li><li><strong>Frontale</strong>: 39 Megapixel</li></ul>
    </div>
    
          </div> <!--TABS END--> <div class="handle"></div>
         </div> <!--TABS-cont END--> 
 
 <div id="mobile1">
 	<h2 class="argom">Caratteristiche tecniche</h2>
<ul><li>Tecnologia <strong>Morphex MK.IV</strong>, quattro forme possibili.</li></ul>
<h3>DISPLAY</h3>
<ul><li>Display <strong>superAMOLED</strong> total surface IPS multi-touch capacitivo, da 2'', 4.3'', 9.7'' e 13'' (rispettivamente orologio, smartphone, tablet, e netbook), risoluzione massima <strong>1440 x 900</strong> a 712 ppi</li></ul>
<h3>FOTOCAMERA</h3>
<ul><li><strong>Principale</strong>: 60 Megapixel</li><li><strong>Frontale</strong>: 39 Megapixel</li></ul>
<h3>HARDWARE</h3>
<ul>
<li><strong>Processore</strong>: ANF Graphite MD1 12 GHz 9-core</li>
<li><strong>Memoria RAM</strong>: 16 Gb</li>
<li><strong>Hard disk</strong>: 2 Tb</li>
<li><strong>Batteria</strong>: Celle di Grafene ad H20</li>
</ul>
<h3>SOFTWARE</h3>
<ul><li>O/S <strong>Chron Futura YZ</strong></li></ul>
<h3>CONNETTIVIT&Agrave;</h3>
<ul>
<li>Wi-Fi</li>
<li>GPS</li>
<li>4G SLTE</li>
<li> Bluetooth 4.0</li>
</ul>
 </div>
          
          <div id="sort">
          <div id="sortin"><!--TABLE SORTER OPEN-->
                	<table id="myTable" class="tablesorter"> 
						<thead> 
							<tr> 
    							<th>Nome</th> 
    							<th>Cognome</th> 
							    <th>Email</th> 
							    <th>Capitale sociale versato</th> 
							    <th>Sito Web</th> 
							</tr> 
						</thead> 
						<tbody> 
							<tr> 
    							<td>Alberto</td> 
    							<td>Reani</td> 
    							<td>alberto.reani@gmail.com</td> 
    							<td>€1000,00</td> 
    							<td>http://www.albertoreani.it/</td> 
							</tr> 
							<tr> 
    							<td>Marco</td> 
    							<td>Stinco</td> 
							    <td>marstweb@gmail.com</td> 
							    <td>€100,00</td> 
    							<td>http://www.marcostinco.it/</td> 
							</tr> 
							<tr> 
    							<td>Antonio</td> 
    							<td>Pagano</td> 
    							<td>antoniopagano83@gmail.com</td> 
    							<td>€10,00</td> 
    							<td>http://www.antoniopagano.it/</td> 
							</tr> 
							<tr> 
 							   <td>Beppe</td> 
 							   <td>Feltrin</td> 
   							   <td>beppefeltrin@gmail.com</td> 
   							   <td>€1,00</td> 
    							<td>http://www.beppefeltrin.it/</td> 
							</tr> 
						</tbody> 
					</table>
                </div>
                <div class="handle2"></div>
            </div><!--TABLE SORTER END--> 


      </section>

      <section class="middle1">
       
       <header id="foto" class="middlehead"> <img src="css/images/camera.gif" alt="foto" class="title_middle" /> </header> 
       
 <div class="flex-container">
       <div class="flexslider">
          <ul class="slides">
             
            
              <li>
                <img src="css/images/4.jpg" alt="image1" />
              </li>
              
              <li>
                <img src="css/images/9.jpg" alt="image2" />
              </li>
              
              <li>
                <img src="css/images/10.jpg" alt="image3" />
              </li>
              
              <li>
                <img src="css/images/11.jpg" alt="image4" />
              </li>
              
              <li>
                <img src="css/images/12.jpg" alt="image5" />
              </li>
              
              <li>
                <img src="css/images/13.jpg" alt="image6" />
              </li>
              
              <li>
                <img src="css/images/14.jpg" alt="image7" />
              </li>

  </ul>
</div>   <!-- flexslider end -->
</div>   <!-- flex-container end -->    
      </section>


      <section class="right1">

      	 <header id="ordina"  class="lefthead2"><h2 class="title_left2" > </h2> </header> 
           <h2 class="pren"> Prenota </h2>
           <p class="ord"><strong>Sosacroniro</strong> è stato annunciato il <strong>14 Luglio 2012</strong> e verrà lanciato sul mercato mondiale entro il <strong>Dicembre 2015</strong>.<br/>
              <strong>Prenotalo</strong> adesso, e lo riceverai 2 mesi in anticipo alla data di uscita!
           </p>

          <div id="ordform" >
      	  <script type="text/javascript" src="http://form.jotformeu.com/jsform/23446423411344"></script>
          </div>
          
         <a href="http://form.jotformeu.com/form/23446423411344" class="inv link"> Prenota ora! </a>

      </section>

       <section class="middle2">
       
       <header id="contatti" class="middlehead2"> <img src="css/images/mail.gif" alt="contatti" class="title_middle2" /> </header> 

       
             
               <form id="formdiv" action="scripts/form_function.php" method="post" onsubmit="return validateThisForm(this)">
                        
                        <p class="input1"><label for="nome" class="lab">Nome&nbsp;</label><input name="nome" type="text" id="nome" class="nome" placeholder="Nome Cognome" /></p>
                        <p><label for="email" class="lab">Email </label><input name="email" type="text" id="email" class="email" placeholder="email@example.com" /></p>
                        <p><label for="tel" class="lab">Telef.&nbsp;</label><input name="tel" type="text" id="tel" class="tel" placeholder="+XX XXX - 456789" /></p>
                        <p><label class="lab" for="testo" id="stupid">Mess.&nbsp;</label><textarea name="testo" id="testo" class="testo" placeholder="Scrivi il tuo messaggio..."></textarea></p>
                        <p><input value="Invia" type="submit" id="invia" class="button"/>
                           <span id="formalert"></span>
                        </p>


                </form>

     
       <div id="contact_panel">
    <h3>Contatti generali</h3>
    <div>
       <p>

       	Synergia s.r.l. <br/><br/>
       	Via del Pratello 9<br/> <br/>
       	40122, Bologna, BO<br/><br/>
       	051 2967683

       </p>
        
    </div>
    
    <h3>Email</h3>
    <div >
        <p id="emai">
     Alberto Reani:<br/> <a href="mailto:alberto.reani@gmail.com" class="link"> alberto.reani@gmail.com </a><br/><br/>
     Marco Stinco: <br/> <a href="mailto:marstweb@gmail.com" class="link"> marstweb@gmail.com </a><br/><br/>
     Antonio Pagano: <br/> <a href="mailto:antoniopagano83@gmail.com" class="link"> antoniopagano83@gmail.com </a><br/><br/>
     Beppe Feltrin:<br/> <a href="mailto:beppefeltrin@gmail.com" class="link"> beppefeltrin@gmail.com </a>
        </p>
    </div>
    
    
    <h3>Blog</h3>
    <div>
    	<p> <a href="http://www.sosacroniro.it/blog/" class="link">Blog Sosacroniro</a></p>
        
    </div>
    <h3>Pagine personali</h3>
    <div>
       <p>
      <a href="people/alberto/index.html" class="link">Alberto Reani</a><br/><br/>
      <a href="people/marco/index.php" class="link">Marco Stinco</a><br/><br/>
      <a href="people/antonio/index.html" class="link">Antonio Pagano</a><br/><br/>
      <a href="people/beppe/index.html" class="link">Beppe Feltrin</a>
        </p>
    </div>
   
    </div>
    
      </section>

   </article>

<footer class="footer">
	
	<p class="copy">
         Copyright&copy;Sosacroniro.it Soul Inside    
	</p>

	<a href="#hidetopsection" class="ease"><img src="css/images/arrowup.gif" alt="To top" class="arrowup" /></a>

</footer>

</div> <!-- main end -->

</div> <!-- wrapper end -->

<script type="text/javascript" src="scripts/scripts.js"></script>
</body>

</html>